import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:myaplikasi/tampilan/edit.dart';
import 'dart:convert';

import 'package:myaplikasi/tampilan/input.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final String url = "http://10.0.2.2/tokoku/ambildata.php";
  Future getData() async {
    var response = await http.get(Uri.parse(url));
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  Future deleteData(String dataId) async {
    final String url = "http://192.168.1.2/api/inputs/" + dataId;
    var response = await http.delete(Uri.parse(url));

    return jsonDecode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("List Perlengkapan"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[Colors.purple, Colors.red]),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => FormInput()));
          }),
      body: FutureBuilder(
        future: getData(),
        builder: (context, data) {
          if (data.hasData) {
            return ListView.builder(
              itemCount: data.data['data'].length,
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.only(top: 10),
                  child: Card(
                    elevation: 10,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 100,
                          width: 100,
                          child: Image.network(
                            data.data['data'][index]['gambar'],
                            fit: BoxFit.cover,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Nama Barang : " +
                                data.data['data'][index]['namaBarang']),
                            Text("Keterangan  : " +
                                data.data['data'][index]['keterangan']),
                            Text("Stok        : " +
                                data.data['data'][index]['stok']),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            IconButton(
                                icon: Icon(Icons.edit),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => EditData(
                                              input: data.data['data']
                                                  [index])));
                                }),
                            IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  AlertDialog alertDialog = new AlertDialog(
                                    content: new Text(
                                        "Apakah Kamu ingin menghapusnya? "),
                                    actions: <Widget>[
                                      new RaisedButton(
                                          child: new Text("HAPUS"),
                                          onPressed: () {
                                            deleteData(data.data['data'][index]
                                                        ['id']
                                                    .toString())
                                                .then((value) {
                                              setState(() {});
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(SnackBar(
                                                      content: Text(
                                                          "Data sudah dihapus")));
                                            });
                                          }),
                                      new RaisedButton(
                                        child: new Text("BATAL"),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                    ],
                                  );
                                })
                          ],
                        )
                      ],
                    ),
                  ),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
