import 'package:ambildata_api/addData.dart';
import 'package:ambildata_api/detailHalaman.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // block standar dari sebuah aplikasi (katanya)
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TOKOKU',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<List> ambilData() async {
    var data =
        await http.get(Uri.parse("http://10.0.2.2/tokoku/ambildata.php"));
    var jsonData = json.decode(data.body);
    return jsonData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "LIST BARANG",
        ),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[Colors.purple, Colors.red]),
          ),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () => Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new addData())),
        child: Icon(Icons.add),
      ),
      body: Container(
        child: FutureBuilder(
          future: ambilData(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return new Container(
                    padding: const EdgeInsets.all(10.0),
                    child: new GestureDetector(
                      onTap: () => Navigator.of(context).push(
                          new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new detailHalaman(
                                    list: snapshot.data,
                                    index: index,
                                  ))),
                      child: new Card(
                        child: new ListTile(
                          title: new Text(
                            snapshot.data[index]['nama'],
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          leading: new Icon(Icons.all_inbox_sharp),
                          subtitle: new Text(
                              "Stok: " + snapshot.data[index]['stock']),
                        ),
                      ),
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}
