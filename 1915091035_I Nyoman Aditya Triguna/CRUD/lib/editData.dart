import 'package:ambildata_api/main.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class editData extends StatefulWidget {
  List list;
  int index;
  editData({required this.list, required this.index});

  @override
  _editDataState createState() => _editDataState();
}

class _editDataState extends State<editData> {
  TextEditingController controllerKode = new TextEditingController();
  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerHarga = new TextEditingController();
  TextEditingController controllerStock = new TextEditingController();

  @override
  void initState() {
    controllerKode.text = widget.list[widget.index]['kode'];
    controllerNama.text = widget.list[widget.index]['nama'];
    controllerHarga.text = widget.list[widget.index]['harga'];
    controllerStock.text = widget.list[widget.index]['stock'];
    super.initState();
  }

  void editData() {
    http.post(Uri.parse("http://10.0.2.2/tokoku/editdata.php"), body: {
      "kodeBarang": controllerKode.text,
      "namaBarang": controllerNama.text,
      "hargaBarang": controllerHarga.text,
      "stockBarang": controllerStock.text,
      "id": widget.list[widget.index]['id'],
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Edit Data"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: new ListView(
          children: <Widget>[
            new Column(
              children: <Widget>[
                new TextField(
                  controller: controllerKode,
                  decoration: new InputDecoration(
                      hintText: "Kode Barang", labelText: "Kode Barang"),
                ),
                new TextField(
                  controller: controllerNama,
                  decoration: new InputDecoration(
                      hintText: "Nama Barang", labelText: "Nama Barang"),
                ),
                new TextField(
                  controller: controllerHarga,
                  decoration: new InputDecoration(
                      hintText: "Harga Barang", labelText: "Harga Barang"),
                ),
                new TextField(
                  controller: controllerStock,
                  decoration: new InputDecoration(
                      hintText: "Stock Barang", labelText: "Stock Barang"),
                ),
                new Padding(padding: const EdgeInsets.all(20.0)),
                new ElevatedButton(
                    onPressed: () {
                      editData();
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context) => new MyHomePage()));
                    },
                    child: new Text("Edit"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
