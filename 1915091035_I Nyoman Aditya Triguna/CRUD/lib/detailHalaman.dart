import 'package:ambildata_api/editData.dart';
import 'package:ambildata_api/main.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class detailHalaman extends StatefulWidget {
  List list;
  int index;
  detailHalaman({required this.list, required this.index});

  @override
  _detailHalamanState createState() => _detailHalamanState();
}

class _detailHalamanState extends State<detailHalaman> {
  void hapusData() {
    http.post(Uri.parse("http://10.0.2.2/tokoku/hapusdata.php"),
        body: {'id': widget.list[widget.index]['id']});
    ;
  }

  void confirm() {
    AlertDialog alertDialog = new AlertDialog(
      content: new Text(
          "Apakah Kamu ingin menghapusnya? '${widget.list[widget.index]['nama']}'"),
      actions: <Widget>[
        new RaisedButton(
            child: new Text("HAPUS"),
            onPressed: () {
              hapusData();
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new MyHomePage()));
            }),
        new RaisedButton(
          child: new Text("BATAL"),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(
          widget.list[widget.index]['nama'],
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[Colors.purple, Colors.red]),
          ),
        ),
      ),
      body: ListView(children: [
        Container(
          padding: EdgeInsets.only(top: 10),
          child: new Card(
            elevation: 10,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget.list[widget.index]['nama'],
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      new Text(
                        "Kode: " + widget.list[widget.index]['kode'],
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      new Text("harga: " + widget.list[widget.index]['harga'],
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      new Text("stock: " + widget.list[widget.index]['stock'],
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                    ],
                  ),
                ),
                new IconButton(
                  onPressed: () =>
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context) => new editData(
                                list: widget.list,
                                index: widget.index,
                              ))),
                  icon: Icon(
                    Icons.edit,
                    color: Colors.blue,
                  ),
                ),
                new IconButton(
                  onPressed: () => confirm(),
                  icon: Icon(
                    Icons.delete,
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
